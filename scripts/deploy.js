const ftp = require('basic-ftp');
const fs = require('fs');

const PROJECTS_DIR_NAME = '/unilimes.com/webgl/project';
const CURRENT_PROJECT_NAME = 'kronos-globe';
const DIST_DIR = 'build';

deploy();

async function deploy() {
    const client = new ftp.Client();
    client.ftp.verbose = true;
    try {
        await client.access({
            host: 'sitc.ftp.ukraine.com.ua',
            user: 'sitc_ftp2',
            password: '7kR5Yr0Cn8'
        });
        await client.cd(`${ PROJECTS_DIR_NAME }`);
        try {
            await client.removeDir(`${ CURRENT_PROJECT_NAME }`);
        } catch (err) {
            console.log(err)
        }
        await client.uploadDir(`./${ DIST_DIR }`, `${ CURRENT_PROJECT_NAME }`)
    } catch (err) {
        console.log(err)
    }
    client.close()
}
