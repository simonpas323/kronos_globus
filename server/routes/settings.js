const router = require('express').Router();
const Settings = require('../models/settings');

router.post('/', function (request, res) {
    Settings.find({}, function (err, list) {
        if (err) {
            res.json({ err })
        } else {
            if (list.length) {
                Settings.updateMany({}, { $set: { settings: request.body } }, function () {
                    res.json({ updated: true })
                });
            } else {
                var settins = new Settings({
                    settings: request.body
                });
                settins.save((function (err) {
                    res.json({ saved: true })
                }))
            }
        }
    })
});

router.get('/', function (request, res) {
    Settings.find({}, function (err, list) {
        if (err) {
            res.json({ err })
        } else {
            res.json({
                data: list.length ? list[ 0 ] : null
            })
        }
    })
});
router.delete('/', function (request, res) {
    Settings.remove({}, function (err, list) {
        if (err) {
            res.json({ err })
        } else {
            res.json({
                data: list[ 0 ]
            })
        }
    })
});

module.exports = router;
