// process.env.NODE_ENV = 'production';selectedChild
const fs = require('fs');

var CONFIG = {
    env: process.env.NODE_ENV,
    FILE_UPLOAD_EXT: [ '.obj', 'image/', '.svg' ],
    FILE_UPLOAD_ATTR: [ 'model[]', 'frames[]', 'alignFrames[]', 'structure', 'preloader[]', 'tooltip[]', 'controls[]', 'svgs[]' ],
    FILE_UPLOAD_ACCEC: parseInt('0777', 8),
    port: process.env.PORT || 3006,
    mongoose: {
        uri: 'mongodb://localhost/globe_demo'
    },

};
module.exports = CONFIG;
