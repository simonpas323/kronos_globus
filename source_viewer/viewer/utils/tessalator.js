import {
    Vector2,
    Sphere,
    Vector3,
    Face3,
    Geometry
} from 'three-full';

export default class Tessalator extends  Geometry {
    constructor(data, type = 'sphere') {
        super();

        this.data = data;
        this.type = type;

        let i, uvs = [];
        const innerRadius = 0.98;

        for (i = 0; i < data.vertices.length; i += 2) {
            const lon = data.vertices[ i ];
            const lat = data.vertices[ i + 1 ];

            let wx = 0;
            let wz = 0;
            let wy = 0;

            if (type === 'plane') {
                wx = lon;
                wz = 0;
                wy = lat;
            } else {
                const phi = +(90.0 - lat) * Math.PI / 180.0;
                const the = +(180.0 - lon) * Math.PI / 180.0;

                wx = Math.sin(the) * Math.sin(phi) * -1;
                wz = Math.cos(the) * Math.sin(phi);
                wy = Math.cos(phi);
            }

            const wu = 0.25 + lon / 360.0;
            const wv = 0.50 + lat / 180.0;

            this.vertices.push(new Vector3(wx, wy, wz));

            uvs.push(new Vector2(wu, wv));
        }

        const n = this.vertices.length;

        if (innerRadius <= 1) {
            for (i = 0; i < n; i++) {
                const v = this.vertices[ i ];
                const p = v.clone();

                if (type === 'plane') {
                    p.z = -innerRadius;
                    this.vertices.push(p);
                } else {
                    this.vertices.push(p.multiplyScalar(innerRadius));
                }
            }
        }

        for (i = 0; i < data.triangles.length; i += 3) {
            const a = data.triangles[ i ];
            const b = data.triangles[ i + 1 ];
            const c = data.triangles[ i + 2 ];

            this.faces.push(new Face3(a, b, c, [ this.vertices[ a ], this.vertices[ b ], this.vertices[ c ] ]));
            this.faceVertexUvs[ 0 ].push([ uvs[ a ], uvs[ b ], uvs[ c ] ]);

            if ((0 < innerRadius) && (innerRadius <= 1)) {
                this.faces.push(new Face3(n + b, n + a, n + c, [
                    this.vertices[ b ].clone()
                        .multiplyScalar(-1), this.vertices[ a ].clone()
                        .multiplyScalar(-1), this.vertices[ c ].clone()
                        .multiplyScalar(-1)
                ]));

                this.faceVertexUvs[ 0 ].push([ uvs[ b ], uvs[ a ], uvs[ c ] ]);
            }
        }

        if (innerRadius < 1) {
            for (i = 0; i < data.polygons.length; i++) {
                const polyWithHoles = data.polygons[ i ];

                for (let j = 0; j < polyWithHoles.length; j++) {
                    const polygonOrHole = polyWithHoles[ j ];

                    for (let k = 0; k < polygonOrHole.length; k++) {
                        const a = polygonOrHole[ k ],
                            b = polygonOrHole[ (k + 1) % polygonOrHole.length ];
                        const va1 = this.vertices[ a ],
                            vb1 = this.vertices[ b ];
                        const va2 = this.vertices[ n + a ],
                            vb2 = this.vertices[ n + b ];
                        let normal;

                        if (j < 1) {
                            normal = vb1.clone().sub(va1).cross(va2.clone().sub(va1)).normalize();

                            this.faces.push(new Face3(a, b, n + a, [ normal, normal, normal ]));
                            this.faceVertexUvs[ 0 ].push([ uvs[ a ], uvs[ b ], uvs[ a ] ]);

                            if (innerRadius > 0) {
                                this.faces.push(new Face3(b, n + b, n + a, [ normal, normal, normal ]));
                                this.faceVertexUvs[ 0 ].push([ uvs[ b ], uvs[ b ], uvs[ a ] ]);
                            }
                        } else {
                            normal = va2.clone().sub(va1).cross(vb1.clone().sub(va1)).normalize();

                            this.faces.push(new Face3(b, a, n + a, [ normal, normal, normal ]));
                            this.faceVertexUvs[ 0 ].push([ uvs[ b ], uvs[ a ], uvs[ a ] ]);

                            if (innerRadius > 0) {
                                this.faces.push(new Face3(b, n + a, n + b, [ normal, normal, normal ]));
                                this.faceVertexUvs[ 0 ].push([ uvs[ b ], uvs[ a ], uvs[ b ] ]);
                            }
                        }
                    }
                }
            }
        }

        this.computeFaceNormals();

        this.boundingSphere = new Sphere(new Vector3(), 1);
    }

    center() {
        const data = this.data;
        const type = this.type;

        let lat = 0;
        let lon = 0;

        for (let i = 0; i < data.vertices.length; i += 2) {
            lon += data.vertices[ i ];
            lat += data.vertices[ i + 1 ];
        }

        lat /= data.vertices.length / 2;
        lon /= data.vertices.length / 2;

        let x = 0;
        let z = 0;
        let y = 0;

        if (type === 'plane') {
            x = lon;
            z = 0;
            y = lat;
        } else {
            const phi = +(90.0 - lat) * Math.PI / 180.0;
            const the = +(180.0 - lon) * Math.PI / 180.0;

            x = Math.sin(the) * Math.sin(phi) * -1;
            z = Math.cos(the) * Math.sin(phi);
            y = Math.cos(phi);
        }

        return new Vector3(x, y, z);
    }
}
