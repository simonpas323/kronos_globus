import lodash from 'lodash';
import TWEEN from '../../tween.js';

import Behaviour from '../../engine/behaviour';

// import LineBufferGeometry from '../../utils/geometry';
// import Tessalator from '../../utils/tessalator';
import { hexToRgbNew } from '../../utils/extra';

import { calcPosFromLatLonRad } from '../../../utils';
import {
    Sprite,
    SpriteMaterial,
    BufferGeometryUtils,
    VertexNormalsHelper,
    LineBasicMaterial,
    Color,
    Vector3,
    TubeBufferGeometry,
    CatmullRomCurve3,
    Geometry,
    MeshBasicMaterial,
    EdgesGeometry,
    LineSegments,
    TextureLoader,
    MeshPhongMaterial,
    BoxHelper,
    SphereBufferGeometry,
    Mesh,
    Group,
    Object3D,
} from 'three-full';

export default class Earth extends Behaviour {
    init(scale) {
        const textureLoader = this.textureLoader = new TextureLoader();

        this.textures = {
            spritePOINTER: textureLoader.load('assets/1517502084.png'),
            spritePOINTER1: textureLoader.load('assets/particleA.png')
        };
        this.scale = scale;

        this.hover = 1.01;
        this.radius = 0.995;
        this.segments = 32;

        ////

        this.mesh = new Group();
        this.mesh.name = 'Earth';

        this.mesh.rotation.z = 0.2;

        //

        this.holder = new Group();
        this.holder.name = 'Holder';

        this.holder.scale.multiplyScalar(this.scale);

        this.mesh.add(this.holder);

        //

        this.sphere = new Mesh(
            new SphereBufferGeometry(this.radius, this.segments, this.segments),
            new MeshPhongMaterial({
                color: '#1d2035',
                // polygonOffset: true,
                // polygonOffsetFactor: 1.0,
                // polygonOffsetUnits: 4.0,
                depthWrite: false,
                transparent: true,
                opacity: 0.0
            })
        );

        this.sphere.name = 'Sphere';
        this.sphere.renderOrder = 1;

        this.ocean = new Mesh(
            new SphereBufferGeometry(this.radius * 0.99, this.segments, this.segments),
            new MeshPhongMaterial({
                color: '#548ba4',
                depthWrite: false,
                // depthTest: false,
                transparent: true,
                opacity: 0.1
            })
        );

        this.ocean.name = 'SphereOcean';
        this.ocean.renderOrder = 1;

        this.edges = new LineSegments(
            new EdgesGeometry(this.sphere.geometry, 0.2),
            new LineBasicMaterial({
                color: '#1d2035',
                // depthTest: false,
                transparent: true,
                opacity: 0.2
            })
        );
        this.edges.name = 'Edges';
        this.edges.renderOrder = 1;

        this.countries = new Group();
        this.countries.name = 'Countries';

        this.helpers = new Group();
        this.helpers.name = 'Helpers';

        this.holder.add(this.helpers);

        this.applyConnections({
            globePoints: []
        })
    }

    start() {
        const object = this.object;
        const material = new MeshPhongMaterial({
            // depthWrite: false,
            // depthTest: false,
            // color: color
        });
        const children = [];
        object.traverse((country) => {
            country.renderOrder = 1;
            if (country.material) {

                if (Array.isArray(country.material)) {
                    for (let i = 0; i < country.material.length; i++) {
                        if (country.material[ i ].name !== 'back_side') {
                            country.material[ i ] = material;
                        }
                    }
                } else {
                    country.material = material;
                }
                country.startPosition = country.position.clone();
                country._helper = new BoxHelper(country);
                country.position.copy(
                    country.position.clone().addScaledVector(
                        country._helper.geometry.boundingSphere.center.sub(this.scene.position).normalize(),
                        Math.random() * 100
                    )
                );
                country.endPosition = country.position.clone();
                children.push(country);
            }
        });
        children.forEach((child) => {
            child.renderOrder = 1;
            this.countries.add(child);

        });
        this.countries.scale.multiplyScalar(0.101);

        this.countries._helper = new BoxHelper(this.countries);

        this.tween = new TWEEN.Tween({ alpha: 0 })
            .to({ alpha: 1 }, 5000)
            .easing(TWEEN.Easing.Cubic.Out)
            .onUpdate(({ alpha }) => {
                this.countries.children.forEach((el) => {
                    el.position.copy(
                        el.endPosition.clone().lerp(el.startPosition, alpha)
                    );
                });
            }).onComplete((props) => {
                this.tween = false;

            });

        this.holder.add(this.ocean);
        this.holder.add(this.edges);
        this.holder.add(this.countries);

    }

    listen() {

    }

    applySettings({ settings, globePoints, dataPoints, selectedConection, el_settings }) {

        if (!this.hasConnections) {
            this.hasConnections = true;
            this.applyConnections({ settings, globePoints, el_settings });
            setTimeout(() => {
                for (const key in el_settings) {
                    this.applySettingsToTube(el_settings, parseInt(key));
                }
                this.applySettings({ settings, globePoints, dataPoints, selectedConection, el_settings });
                this.animate();
                this.tween.start();
            });
        }

        if (
            this.countries.countryBackColor !== settings.main.countryBackColor ||
            this.countries.countryColor !== settings.main.countryColor ||
            this.countries.countryOpacity !== settings.main.countryOpacity
        ) {
            this.countries.countryBackColor = settings.main.countryBackColor;
            this.countries.countryOpacity = settings.main.countryOpacity;
            this.countries.countryColor = settings.main.countryColor;
            this.countries.children.forEach((country) => {
                if (Array.isArray(country.material)) {
                    country.material.forEach((mat) => {

                        if (mat.name === 'back_side') {
                            mat.color.setRGB(...hexToRgbNew(settings.main.countryBackColor));
                        } else {
                            mat.color.setRGB(...hexToRgbNew(settings.main.countryColor));
                        }

                        mat.transparent = settings.main.countryOpacity < 100;
                        mat.opacity = settings.main.countryOpacity / 100;
                    })
                } else {
                    country.material.color.setRGB(...hexToRgbNew(settings.main.countryColor));
                    country.material.transparent = settings.main.countryOpacity < 100;
                    country.material.opacity = settings.main.countryOpacity / 100;
                    country.material.needsUpdate = true;
                }
            });
        }
        this.ocean.material.color = new Color(settings.main.oceanColor);
        this.ocean.material.opacity = settings.main.oceanOpacity / 100;
        this.ocean.material.needsUpdate = true;
        this.ocean.material.depthWrite = this.ocean.material.opacity === 1;

        this.edges.material.color = new Color(settings.main.meridianColor);
        this.edges.material.opacity = settings.main.meridianOpacity / 100;
        this.edges.visible = settings.main.meridianVisible;
        this.edges.material.needsUpdate = true;

        this.SPEED = settings.main.pointRotateSpeed / 10;

        this.filterVisibileElements(settings, selectedConection);

        if (this.lastSelectedTube) {
            [ ...this.connections.children ].forEach((_tube) => {
                _tube.geometry = _tube.origin_geometry;
            });
            this.lastSelectedTube = false;
        }
        if (!selectedConection || !el_settings[ selectedConection ] || selectedConection === 'ALL') {
            return
        }
        this.applySettingsToTube(el_settings, selectedConection, true, settings.main.pointRotateSpeed / 10)

    }

    filterVisibileElements(settings, selectedConection) {
        const COUNT = settings.main.countOfActive / 100;
        const IsAll = 'ALL' === settings.main.tierFilter;
        const visibleElements = this.connections.children.filter((tube) => {
            if (tube.name === 'Plane') return false;
            const visible = (
                (IsAll && !this._admin()) ||
                tube._connection.tier === (settings.main.tierFilter)
                || (this._admin() && tube._connection.tier === selectedConection)
            ) && !(selectedConection === 'NONE');
            tube.visible = !!visible;
            tube._SPEED = settings.main.pointRotateSpeed / 10;
            return visible
        });
        visibleElements.forEach((child, index) => {
            if (child.material) {
                child.visible = index / visibleElements.length < COUNT;
            }
        });
    }

    _admin() {
        return location.href.match('admin');
    }

    applySettingsToTube(el_settings, selectedConection) {
        if (isNaN(selectedConection)) return;
        const _settings = el_settings[ selectedConection ];
        const {
            pointColor,
            pointOpacity,
            pointSpeed,
            pointScale,
        } = _settings;
        this.lastSelectedTube = selectedConection;
        for (let i = 0, list = [ ...this.connections.children ]; i < list.length; i++) {
            let _tube = list[ i ];
            // let _tube = this.connections._tubes[ selectedConection ];
            if (_tube._connection.tier === (selectedConection)) {
                if (_tube.CONNECTION_HEIGHT !== _settings.height) {
                    if (_tube.parent) {
                        _tube.parent.remove(_tube);
                    }
                    _tube = this._addConnection(_tube._connection, { connection: _settings }, _tube._index);
                    _tube.CONNECTION_HEIGHT = _settings.height;
                }

                // _tube._SPEED = pointSpeed / 10;
                if (pointColor !== this.textures.spritePOINTER2)
                    for (let j = 0; j < _tube.points.children.length; j++) {
                        const point = _tube.points.children[ j ];
                        point.material.color = new Color(pointColor);
                        point.material.opacity = pointOpacity / 100;
                        point.scale.set(pointScale / 100, pointScale / 100, pointScale / 100);
                    }
                // if (selection) _tube.geometry = _tube._hugeGeo;
                _tube.material.color = (new Color()).setRGB(...hexToRgbNew(_settings.color));
                _tube.material.opacity = _settings.opacity / 100;
                // if(this._admin()) {
                // _tube.visible = _settings.visible;
                // }
            }
        }

    }

    applyConnections({ settings, globePoints, el_settings }) {
        this.connections = new Object3D();
        this.connections.rotation.y = Math.PI / 2;//-2.13;
        if (!settings) return;
        this.holder.add(this.connections);
        globePoints.forEach((connection, index) => {
            this._addConnection(connection, settings || { connection: el_settings[ connection.tier ] }, index);
        });
    }

    setArc3D(pointStart, pointEnd, HEIGHT, smoothness = 200, color = 'lime', clockWise = false) {
        // calculate normal
        const cb = new Vector3(),
            center = new Vector3(),
            ab = new Vector3(),
            normal = new Vector3();
        cb.subVectors(new Vector3(), pointEnd);
        ab.subVectors(pointStart, pointEnd);
        cb.cross(ab);
        normal.copy(cb).normalize();

        // get angle between vectors
        let angle = pointStart.angleTo(pointEnd);
        if (clockWise) angle = angle - Math.PI * 2;
        const angleDelta = angle / (smoothness - 1);

        const geometry = new Geometry();
        // geometry.vertices.push(originPositionStart.clone());
        for (let i = 0, dir = 1, step = 0, speed = 1, prevHeight = 0; i < smoothness; i++) {
            const point = pointStart.clone().applyAxisAngle(normal, angleDelta * i);
            let height = (((step) / smoothness * 0.5) * 0.03 * HEIGHT) * speed;

            if (i > smoothness / 2 && dir > 0) {
                dir = -1;
                height = prevHeight;
            }
            if (i > 0 && i < smoothness - 1) {
                point.addScaledVector(point.clone().sub(center).normalize(), height);
            }

            step += speed * dir;
            speed = (1 - step / (smoothness * 0.75));
            geometry.vertices.push(point);
            prevHeight = height;
        }
        return geometry;
    }

    _addConnection(connection, settings, index) {
        const HEIGHT = settings.connection.height;
        const {
            pointColor,
            pointOpacity,
            pointSpeed,
            pointScale,
        } = settings.connection;
        const { radius } = this;///.countries._helper.geometry.boundingSphere;
        const _distance = radius * 1.01;//* 0.59;

        const originPosition = new Vector3(...calcPosFromLatLonRad(connection.start.lat, connection.start.lon, _distance));
        const targetPosition = new Vector3(...calcPosFromLatLonRad(connection.end.lat, connection.end.lon, _distance));

        const curve = new CatmullRomCurve3(this.setArc3D(originPosition, targetPosition, HEIGHT).vertices);
        const color = new Color('#ff2a39');
        const tube = new Mesh(
            new TubeBufferGeometry(curve, 64, radius * 0.001, 3, false),
            new MeshBasicMaterial({
                color,
                transparent: true,
                depthWrite: false
            })
        );
        tube.renderOrder = 5;
        tube._hugeGeo = new TubeBufferGeometry(curve, 64, radius * 0.005, 3, false);
        tube.origin_geometry = tube.geometry;
        tube._SPEED = this.SPEED;
        tube._index = index;
        tube._connection = connection;
        tube.geometry.setDrawRange(0, tube._connection.isDrawed ? Infinity : 0);
        tube._path = curve.getPoints(curve.arcLengthDivisions * 5);
        // tube._SPEED = pointSpeed / 10;

        const countOfPoint = 2,
            points = tube.points = new Object3D();
        const mat = new MeshBasicMaterial({
            color: pointColor,
            transparent: true,
            depthWrite: false
        });
        // const material = new SpriteMaterial({
        //     map:  this.textures.spritePOINTER,
        //     color: pointColor,
        //     transparent: true,
        //     depthWrite: false,
        //     depthTest: true,
        //     opacity: pointOpacity / 100
        // });
        for (let i = 0; i < countOfPoint; i++) {
            const point = new Mesh(new SphereBufferGeometry(0.1, 32, 32), mat);
            // const point = new Sprite(
            //     material
            // );
            point.scale.multiplyScalar(pointScale / 100);
            point.visible = false;
            points.add(point);
            point._index = 0;
            point.renderOrder = 5;
        }

        this.connections.add(tube);
        if (!this.connections._tubes) this.connections._tubes = {};
        this.connections._tubes[ connection.id ] = tube;
        tube.add(points);
        return tube;
    }

    update = (delta) => {
        this.connections.visible = false;
        if (this.tween) {
            return
        }
        this.connections.visible = true;
        this.connections.children.forEach((tube) => {
            if (tube.name === 'Plane' || !tube._connection) return;
            if (tube._connection.isDrawed) {
                if (window.onFinishAnimate) {
                    window.onFinishAnimate();
                    window.onFinishAnimate = false;
                }
                tube.children.forEach((points) => {
                    points.children.forEach((point, index) => {
                        if (index > 0 && point._index === 0 && points.children[ index - 1 ]._index < 500) return;
                        const _index = Math.round(point._index),
                            _v = tube._path[ _index ];
                        if (!_v) {
                            point._index = 0;
                        } else {
                            if (_index === point.prevIndex) {

                            } else {
                                point.position.copy(_v);
                                point.prevIndex = _index;
                                point.visible = true;
                            }
                            point._index += tube._SPEED;
                        }
                    })
                })

            } else {
                tube.geometry.setDrawRange(0, tube.geometry.drawRange.count + tube._SPEED);
                if (tube.geometry.drawRange.count >= tube.geometry.index.count) {
                    tube._connection.isDrawed = true;
                }
            }
        });
        // this.cloud.rotation.y += 0.001;
    }

    animate() {
    }

    drawCanvas(color = '#ff0000', reset) {
        if (reset || this.textures.spritePOINTER12) {
            return this.textures.spritePOINTER12;
        }
        const canvas = document.createElement('canvas');
        const context = canvas.getContext('2d');
        const centerX = canvas.width / 2;
        const centerY = canvas.height / 2;
        const radius = 70;

        context.beginPath();
        context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
        context.fillStyle = color;
        this.textures.spritePOINTER12 = this.textureLoader.load(canvas.toDataURL());
        return this.textures.spritePOINTER12;
    }
}
