import Behaviour from './engine/behaviour';
import Earth from './components/earth';
import { Object3D, Vector3 } from 'three-full';

import {
    SpotLight,
    SpotLightHelper,
    MeshBasicMaterial,
    Mesh,
    DirectionalLightHelper,
    SphereBufferGeometry,
    DirectionalLight,
    AmbientLight, Color,
} from 'three-full';
import { hexToRgbNew } from './utils/extra';

export default class Viewer extends Behaviour {
    init() {
        this.camera.position.z = -200;
        this.camera.position.set(
            147.59203225453052,
            133.32328161071226,
            -21.011772856533366
        );

        this.controls.minDistance = 85;
        this.controls.maxDistance = 1000;

        ////

        this.renderer.setClearColor(0x333333, 0);

    }

    start() {
        this.size = 100;
        this.earth = new Earth(this.size, this.object);
        this.engine.on('update', () => {
            this.earth.update();
        });
        this.engine.on('keyup', (event) => {
            const control = this.engine.transformControls;
            switch (event.keyCode) {

                case 81: // Q
                    control.setSpace(control.space === 'local' ? 'world' : 'local');
                    break;

                case 87: // W
                    control.setMode('translate');
                    break;

                case 69: // E
                    control.setMode('rotate');
                    break;

                case 82: // R
                    control.setMode('scale');
                    break;

                case 187:
                case 107: // +, =, num+
                    control.setSize(control.size + 0.1);
                    break;

                case 189:
                case 109: // -, _, num-
                    control.setSize(Math.max(control.size - 0.1, 0.1));
                    break;

            }
        });
        this.engine.on('mouseup', () => {

            if (this.lastHovered) {
                this.engine.transformControls.attach(this.lastHovered.object._container);
                this.engine.transformControls.visible = true;
            } else {
                this.engine.transformControls.visible = false;

            }
        });
        this.engine.on('mousemove', (e) => {
            document.body.style.cursor = '';
            let intersects = [];
            if (this.engine.controls.enabled) {
                intersects = this.engine.raycaster.hit(e, this.light);
                if (intersects.length) {
                    document.body.style.cursor = 'pointer';
                }
            }

            this.lastHovered = intersects[ 0 ];
        });
        this.engine.transformControls.addEventListener('mouseUp', () => {
            const light = this.engine.transformControls.object.children[ 0 ];
            this.engine.scene.updateMatrixWorld();
            light.parent.updateMatrixWorld();
            light._globalPosition = new Vector3();
            light._globalPosition.setFromMatrixPosition(light.matrixWorld);
            this.emit('changeLight', light)
        });
        this.scene.add(this.earth.mesh);

    }

    applySettings(opt) {
        this.engine.renderer.setClearColor((new Color()).setRGB(...hexToRgbNew(opt.settings.main.backgroundColor)));
        this.engine.controls.autoRotateSpeed = parseFloat(opt.settings.main.earthRotateSpeed);
        this.earth.applySettings(opt);
        this.applyLightSettings(opt);

    }

    applyLightSettings(opt) {
        const isAdmin = location.href.match('admin');

        if (this.light) {
            this.light.children = [];
        } else {
            this.light = new Object3D();
            this.scene.add(this.light);
        }

        opt.settings.lights.forEach((light) => {
            let _light, helper;
            switch (light.id) {
                case 1: {
                    _light = new AmbientLight(light.color, light.intensity);
                    break;
                }
                case 2: {
                    _light = new DirectionalLight(light.color, light.intensity);
                    helper = new Mesh(
                        new SphereBufferGeometry(this.size * 0.1, 6, 6),
                        new MeshBasicMaterial({
                            wireframe: true,
                            color: light.color
                        }));
                    break;
                }
                case 3: {
                    _light = new SpotLight(light.color, light.intensity);
                    helper = new Mesh(
                        new SphereBufferGeometry(this.size * 0.1, 6, 6),
                        new MeshBasicMaterial({
                            wireframe: true,
                            color: light.color
                        }));

                    _light.angle = Math.PI / 4;
                    _light.penumbra = 0.05;
                    _light.decay = 2;
                    _light.distance = this.controls.minDistance * 2;
                    break;
                }
            }
            if (!_light) return;
            if (light.position) {
                _light.position.copy(light.position);
            }

            if (isAdmin && helper) {
                helper.position.copy(_light.position);
                helper._container = new Object3D();
                helper._container.add(helper);
                helper._container.add(_light);
                helper._settings = light;
                this.light.add(helper._container);
            } else {
                this.light.add(_light);
            }
        })
    }

    listen() {
        // event listeners
    }

    onDestroy() {
        this.earth.destroy();
        super.destroy();
    }
}
