import TWEEN from '../tween.js';
import {
    TransformControls,
    Scene,
    Clock,
    WebGLRenderer,
    PCFSoftShadowMap,
    PerspectiveCamera,
    OrbitControls,
} from 'three-full';

import Events from './events.js';
import Raycaster from './raycaster.js';

export default class Engine extends Events {
    constructor(selector) {
        super();

        // Init
        this.mode = 'normal';

        this.container = (typeof selector === 'string' ? document.querySelector(selector) : selector) || document.body;
        this.container.style.overflow = 'hidden';

        this.width = this.container.offsetWidth;
        this.height = this.container.offsetHeight;

        // Scene
        this.scene = new Scene();
        this.scene.name = 'Scene';

        // Clock
        this.clock = new Clock();

        // Renderer
        this.renderer = new WebGLRenderer({
            // preserveDrawingBuffer: true,
            antialias: true,
            alpha: true
        });
        // this.stats = new Stats();
        // this.container.appendChild(this.stats.dom );
        // this.renderer.setClearColor(0xfefefe, 0.0);
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(this.width, this.height);

        // this.renderer.shadowMap.enabled = false;
        // this.renderer.shadowMap.renderSingleSided = false;
        // this.renderer.shadowMap.renderReverseSided = false;
        // this.renderer.shadowMap.type = PCFSoftShadowMap;

        // this.renderer.autoClear = false;
        // this.renderer.sortObjects = false;
        // this.renderer.gammaInput = true;
        // this.renderer.gammaOutput = true;

        this.renderer.domElement.style.backgroundColor = '#fffefe';
        this.container.appendChild(this.renderer.domElement);

        // Camera
        this.camera = new PerspectiveCamera(75, this.width / this.height, 1, 5000);
        this.camera.name = 'Camera';
        this.camera.position.set(0, 0, -10);
        this.scene.add(this.camera);

        // Controls
        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.controls.autoRotate = true;
        this.controls.autoRotateSpeed = 0.4;
        this.controls.rotateSpeed = 0.3;
        this.controls.device = false;
        this.controls.inverse = false;
        this.controls.enableDamping = true;
        this.controls.dampingFactor = 0.25;
        this.controls.target.set(0, 0, 0);
        this.controls.update();

        this.transformControls = new TransformControls(this.camera, this.renderer.domElement);
        this.transformControls.addEventListener('mouseDown', () => {
            this.controls.enabled = false;
        });
        this.transformControls.addEventListener('mouseUp', () => {
            this.controls.enabled = true;
        });
        this.scene.add(this.transformControls);
        this.transformControls.visible = false;
        // Raycaster
        this.raycaster = new Raycaster(this.camera, this.renderer);

        // Function
        this.listen();
        this.render();
        this.debug();
    }

    listen() {
        const DOM = this.renderer.domElement.parentNode;
        // DefaultLoadingManager.onStart = () => this.emit('start');
        // DefaultLoadingManager.onProgress = (item, loaded, total) => this.emit('progress', (loaded / total * 100), item, loaded, total);
        // DefaultLoadingManager.onLoad = () => this.emit('load');
        // DefaultLoadingManager.onError = () => this.emit('error');
        //
        // this.renderer.domElement.addEventListener('click', (event) => this.emit('click', event), false);
        // this.renderer.domElement.addEventListener('dblclick', (event) => this.emit('dblclick', event), false);
        // this.renderer.domElement.addEventListener('contextmenu', (event) => this.emit('contextmenu', event), false);
        //
        // //this.renderer.domElement.addEventListener('mouseout', (event) => this.emit('mouseout', event), false);
        DOM.addEventListener('mouseup', (event) => this.emit('mouseup, pointerup', event), false);
        DOM.addEventListener('touchend', (event) => this.emit('touchend, pointerup', event), false);
        // this.renderer.domElement.addEventListener('touchcancel', (event) => this.emit('touchcancel', event), false);
        // this.renderer.domElement.addEventListener('touchleave', (event) => this.emit('touchleave', event), false);
        //
        DOM.addEventListener('mousedown', (event) => this.emit('mousedown, pointerdown', event), false);
        DOM.addEventListener('touchstart', (event) => this.emit('touchstart, pointerdown', event), false);
        //\
        DOM.addEventListener('mousemove', (event) => this.emit('mousemove, pointermove', event), false);
        DOM.addEventListener('touchmove', (event) => this.emit('touchmove, pointermove', event), false);
        //
        // window.addEventListener('keydown', (event) => this.emit('keydown', event), false);
        window.addEventListener('keyup', (event) => this.emit('keyup', event), false);
        //
        // window.addEventListener('orientationchange', () => {
        //     this.resize();
        //     this.emit('orientationchange');
        // }, false);
        //
        window.addEventListener('resize', () => {
            this.resize();
            this.emit('resize');
        }, false);

    }

    resize(width, height) {
        this.width = width || this.container.offsetWidth;
        this.height = height || this.container.offsetHeight;

        this.renderer.setPixelRatio(window.devicePixelRatio);

        this.camera.aspect = this.width / this.height;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize(this.width, this.height);
    }

    render() {
        this.renderer.render(this.scene, this.camera);
        window.requestAnimationFrame(() => {
            TWEEN.update();
            this.controls.update();
            this.emit('update');
            this.render();
        });
    }

    debug() {
        window.scene = this.scene;
        window.camera = this.camera;
        window.controls = this.controls;
        window.renderer = this.renderer;
    }
}
