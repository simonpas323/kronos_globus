import Viewer from './index'
import { FBXLoader } from 'three-full';

const DEF_SETTINGS = {
    main: {
        pointRotateSpeed: 100,
        earthRotateSpeed: 0.1,
        meridianColor: '#1d2035',
        meridianVisible: true,
        meridianOpacity: 20,
        countryBackColor: '#ffffff',
        countryColor: '#ffffff',
        countryOpacity: 100,
        tierFilter: 'ALL',
        countryFilter: 'US',
        backgroundColor: '#ffffff',
        countOfActive: 2,
        oceanColor: '#548ba4',
        oceanOpacity: 10
    },
    lights: [
        {
            id: 1,
            uuid: 1,
            color: '#e1e1e1',
            name: 'AreaLight',
            intensity: 0.7,
        },
        {
            id: 2,
            uuid: 2,
            color: '#e1e1e1',
            name: 'DirectionLight',
            intensity: 0.4,
            position: {
                x: 0,
                z: 0,
                y: 1
            },
        }
    ],
    connection: {
        color: '#ff0000',
        height: 0.2,
        visible: true,
        opacity: 100,
        pointColor: '#ff0000',
        pointOpacity: 70,
        pointSpeed: 100,
        pointScale: 5
    },
    controls: {
        rotateSpeed: 0.1
    }
};

function loadMedata() {
    const urls = [
        'assets/capital_one_supply_chain.json',
        // 'assets/capital_one_messy_supply_chain.json',
    ];
    const payload = {};
    return new Promise((resolve) => {
        Promise.all(urls.map(url =>
            fetch(url).then(resp => resp.json())
        )).then(resp => {
            const nodes = [], links = [];

            resp.forEach((el) => {
                nodes.push(...el.nodes);
                links.push(...el.links);
            });
            const dataPoints = {
                nodes,
                links
            }
            payload.dataPoints = dataPoints;
            if (dataPoints) {
                const tiers = [];
                payload.tiers = [ {
                    id: 'ALL'
                } ];
                payload.globePoints = dataPoints.links.map((link, index) => {
                    let buyer, seller;
                    for (let i = 0; i < dataPoints.nodes.length; i++) {
                        if (dataPoints.nodes[ i ].seller_id === link.seller_id) {
                            seller = dataPoints.nodes[ i ];
                            if (buyer) break;
                        } else if (dataPoints.nodes[ i ].seller_id === link.buyer_id) {
                            buyer = dataPoints.nodes[ i ];
                            if (seller) break;
                        }
                    }

                    if (
                        !seller ||
                        !buyer ||
                        !seller.address ||
                        !seller.address.coords ||
                        !buyer.address ||
                        !buyer.address.coords
                    ) return false;
                    return {
                        id: `el-${ index }`,
                        ...link,
                        connectName: `${ seller.seller_name }-${ buyer.seller_name }`,
                        start: {
                            ...seller.address.coords
                        },
                        end: {
                            ...buyer.address.coords
                        }

                    }
                }).filter((el) => el);//.splice(0,10);
            }
        }).catch((e) => {
            console.log(e);
        }).finally(() => resolve(payload))
    })
}

class Main {
    constructor() {
        loadMedata().then((payload) => {
            (new FBXLoader()).load('assets/untitled.fbx', (object) => {
                this.object = object;
                object.children[0].scale.multiplyScalar(0.007);
                this.viewer = new Viewer(document.getElementById('app'), object);
                this.viewer.applySettings({
                    ...payload,
                    settings: DEF_SETTINGS,
                    el_settings: {}
                });
            });
        })

    }
}

document.addEventListener('DOMContentLoaded', function () {
    new Main();
});
