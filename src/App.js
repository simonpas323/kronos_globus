import React, { Component } from 'react';
import { HashRouter  as Router, Switch, Route, Redirect } from 'react-router-dom';
import store from './redux/store';
import { Provider } from 'react-redux';
import { Layout } from 'antd';
import HomePage from './screens/home';
import Admin from './screens/admin';
const { Content  } = Layout;

import './App.css';

class App extends Component {
    render = () => {
        return (
            <Provider store={ store }>
                <Router >
                    <Layout>
                        <Content
                            style={ {
                                overflow: 'initial',
                                minHeight: '100vh',
                            } }
                        >
                            <Switch>
                                <Route exact path="/" component={ HomePage }/>
                                <Route exact path="/admin" component={ Admin }/>
                            </Switch>
                        </Content>
                    </Layout>
                </Router>
            </Provider>
        );
    }
}

export default App;
