import React from 'react';
import GlobeViewer from '../../components/globe.viewer';
import './index.scss'
export default class HomePage extends React.Component {
    render() {
        return (
            <div className="home globe-viewer">
                <GlobeViewer/>
            </div>
        )
    }
}
