import {
    Form, Icon, Input, Button, Checkbox,Row, Col,message
} from 'antd';
import React from 'react';
import { load, logIn } from '../../../redux/actions/user';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class NormalLoginForm extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (err) {
                return
            }else if(values.userName !='swell' || values.password !='swell'){
                return message.error('Check with admin/admin');
            }
            this.props.logIn(values)
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Row type="flex" justify="center" align="center" style={ { height:'100vh' } }>
                <Col span={ 4 }  style={ { display:'flex',alignItems:'center' } }>
                    <Form onSubmit={ this.handleSubmit } className="login-form">
                        <Form.Item>
                            {getFieldDecorator('userName', {
                                rules: [ { required: true, message: 'Please input your username!' } ],
                            })(
                                <Input prefix={ <Icon type="user" style={ { color: 'rgba(0,0,0,.25)' } }/> }
                                       placeholder="Username"/>
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [ { required: true, message: 'Please input your Password!' } ],
                            })(
                                <Input prefix={ <Icon type="lock" style={ { color: 'rgba(0,0,0,.25)' } }/> } type="password"
                                       placeholder="Password"/>
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('remember', {
                                valuePropName: 'checked',
                                initialValue: true,
                            })(
                                <Checkbox>Remember me</Checkbox>
                            )}
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                Log in
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        );
    }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(NormalLoginForm);

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        logIn
    }, dispatch)
);
export default connect(null, mapDispatchToProps)(WrappedNormalLoginForm);
