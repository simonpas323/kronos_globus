import React from 'react';
import { Slider, Switch, Form, Input, Select, Button } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeSettings, changeLineSettings, onResetSettings, onSaveSettings } from '../../../../redux/actions/viewer';
import LightListSettings from './light.add';
import ConnectionSettings from './connection';
import Card from '../../../../components/Card';
import ImgOpacity from '../../../../assets/img/Icon_opacity.png';
import '../index.scss';

const Option = Select.Option;
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};

export class ViewerSettings extends React.Component {

    constructor(p) {
        super(p);
        this.state = {
            saving: false
        }
    }

    componentDidMount() {
        window.onFinishAnimate = () => {
            this.selectConnection(1);
        }
    }

    onChange = (key, val, setting = 'connection') => {
        this.props.changeSettings({
            ...this.props.settings,
            [ setting ]: {
                ...this.props.settings[ setting ],
                [ key ]: val
            }
        });
    };
    selectConnection = (e) => {
        this.props.changeLineSettings(e);
    };
    onSave = () => {
        this.setState({
            saving: true
        });
        this.props.onSaveSettings({
            settings: this.props.settings,
            el_setings: this.props.el_settings
        });
        setTimeout(() => {
            this.setState({
                saving: false
            })
        }, 5000)
    };
    onReset = () => {
        this.props.onResetSettings();
    };

    render() {
        const { connection, controls, main, light } = this.props.settings;
        const {
            meridianVisible,
            meridianColor,
            meridianOpacity,
            countryOpacity,
            countOfActive,
            countryColor,
            earthRotateSpeed,
            pointRotateSpeed,
            backgroundColor,
            oceanColor,
            oceanOpacity
        } = main;
        const { getFieldDecorator } = this.props.form;
        const { opacity, color } = connection;
        return (
            <div key={ this.props.selectedConection }>
                <Form { ...formItemLayout } style={ { 'maxHeight': 'calc(100vh - 150px)', overflow: 'auto' } }
                      className={ 'main-form' }>
                    <div className={ 'row' }>
                        <Card title="Active connections" className={ 'col-4' }>

                            <h3 className={ 'wrap-text' }>Count of connections</h3>
                            <div style={ { width: '100%', textAlign: 'right' } }>
                                <img src={ ImgOpacity } style={ { width: 36 } }/>
                            </div>
                            <div className={ 'slider' }>
                                <Slider
                                    defaultValue={ countOfActive }
                                    onChange={ (val) => this.onChange('countOfActive', val, 'main') }
                                />
                                <div className={ 'd-flex s-b text-info' }>
                                    <span>0</span>
                                    <span>100</span>
                                </div>
                            </div>
                        </Card>

                        <Card title="Lines" className={ 'col-8 connections-card' }>
                            <Select
                                defaultValue={ this.props.selectedConection }
                                onChange={ this.selectConnection }
                            >
                                {
                                    [
                                        { id: 'NONE' }, ...this.props.tiers
                                        .filter((item) => item.id !== 'ALL')
                                    ]
                                        .map(item => <Option key={ item.id }
                                                             value={ item.id }>{item.id}</Option>)
                                }
                            </Select>
                            <div className={ 'row d-inline' }>

                                <div className={ 'slider col-7' }>
                                    <Slider
                                        defaultValue={ opacity }
                                        onChange={ (val) => this.onChange('opacity', val) }
                                    />
                                    <div className={ 'd-flex s-b text-info' }>
                                        <span>0</span>
                                        <span>100</span>
                                    </div>
                                </div>
                                <div className={ 'slider col-5' }>
                                    <div className={ 'd-flex  d-c' }>
                                        <div>Line Color</div>
                                        {getFieldDecorator('color', {
                                            initialValue: color
                                        })(
                                            <Input
                                                onChange={ (e) => this.onChange('color', e.target.value) }
                                                type={ 'color' }
                                            />
                                        )}
                                    </div>
                                </div>
                            </div>

                        </Card>
                    </div>
                    <div className={ 'row' }>
                        <Card title="Country" className={ 'col-4' }>
                            <div className={ 'row d-inline' }>
                                <div className={ 'form-item col-5' }>
                                    <label>Color</label>
                                    <Input
                                        defaultValue={ countryColor }
                                        onChange={ (e) => this.onChange('countryColor', e.target.value, 'main') }
                                        type={ 'color' }
                                    />
                                </div>

                                <div className={ 'form-item col-7 p-0   ' }>
                                    <label className={ ' col-12 p-0' }>Opacity</label>
                                    <div className={ 'slider col-12 p-0' }>
                                        <Slider
                                            defaultValue={ countryOpacity }
                                            onChange={ (val) => this.onChange('countryOpacity', val, 'main') }
                                        />
                                        <div className={ 'd-flex s-b text-info' }>
                                            <span>0</span>
                                            <span>100</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </Card>
                        <Card title="Ocean" className={ 'col-4' }>
                            <div className={ 'row d-inline' }>
                                <div className={ 'form-item col-5' }>
                                    <label>Color</label>
                                    <Input
                                        defaultValue={ oceanColor }
                                        onChange={ (e) => this.onChange('oceanColor', e.target.value, 'main') }
                                        type={ 'color' }
                                    />
                                </div>
                                <div className={ 'form-item row col-7 p-0 ' }>
                                    <label className={ ' col-12 p-0' }>Opacity</label>
                                    <div className={ 'slider col-12 p-0' }>
                                        <Slider
                                            defaultValue={ oceanOpacity }
                                            onChange={ (val) => this.onChange('oceanOpacity', val, 'main') }
                                        />
                                        <div className={ 'd-flex s-b text-info' }>
                                            <span>0</span>
                                            <span>100</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Card>
                        <Card title="Meridians" className={ 'col-4' }>
                            <div className={ 'row d-inline' }>
                                <div className={ 'form-item col-3' }>
                                    <label> Color</label>
                                    <Input
                                        defaultValue={ meridianColor }
                                        onChange={ (e) => this.onChange('meridianColor', e.target.value, 'main') }
                                        type={ 'color' }
                                    />
                                </div>
                                <div className={ 'form-item col-2' }>
                                    <label>Visible</label>
                                    <Switch
                                        onChange={ (val) => this.onChange('meridianVisible', val, 'main') }
                                        size="large"
                                        checked={ meridianVisible }
                                    />
                                </div>
                                <div className={ 'form-item row   col-7 p-0' }>
                                    <label className={ ' col-12 p-0' }> Opacity</label>
                                    <div className={ 'slider col-12 p-0' }>
                                        <Slider
                                            defaultValue={ meridianOpacity }
                                            onChange={ (val) => this.onChange('meridianOpacity', val, 'main') }
                                        />
                                        <div className={ 'd-flex s-b text-info' }>
                                            <span>0</span>
                                            <span>100</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Card>
                    </div>
                    <div className={ 'row' }>
                        <Card title="Lines" className={ 'col-12' }>
                            <ConnectionSettings/>
                        </Card>
                    </div>

                    <div className={ 'row' }>
                        <Card title="Main" className={ 'col-12' }>
                            <div className={ 'row d-inline' }>
                                <div className={ 'col-2 p-0' }>
                                    <div className={ 'form-item' }>
                                        <label>Background Color  </label>
                                        <Input
                                            defaultValue={ backgroundColor }
                                            onChange={ (e) => this.onChange('backgroundColor', e.target.value, 'main') }
                                            type={ 'color' }
                                        />
                                    </div>
                                </div>

                                <div className={ ' col-5 p-0' }>
                                    <div className={ 'form-item row  d-inline' }>
                                        <label className={ ' col-5 p-x' }>Globe speed</label>
                                        <div className={ 'slider col-7 p-x' }>
                                            <Slider
                                                defaultValue={ earthRotateSpeed }
                                                onChange={ (val) => this.onChange('earthRotateSpeed', val, 'main') }
                                            />
                                            <div className={ 'd-flex s-b text-info' }>
                                                <span>Slow</span>
                                                <span>Quick</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={ ' col-5 p-0' }>
                                    <div className={ 'form-item row  d-inline' }>
                                        <label className={ ' col-5 p-x' }>Point speed</label>
                                        <div className={ 'slider col-7' }>
                                            <Slider
                                                defaultValue={ pointRotateSpeed }
                                                onChange={ (val) => this.onChange('pointRotateSpeed', val, 'main') }
                                            />
                                            <div className={ 'd-flex s-b text-info' }>
                                                <span>Slow</span>
                                                <span>Quick</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </Card>

                    </div>
                    <div className={ 'row' }>
                        <LightListSettings/>
                    </div>

                </Form>

                {
                    this.state.saving && <div className={ 'saving' }>Settings Updated</div>
                }
                <div className={ 'btns-action' }>
                    <Button type="primary" onClick={ this.onSave }>
                        Save
                    </Button>
                    <Button onClick={ this.onReset }>
                        Reset
                    </Button>
                </div>
            </div>

        )
    }
}

const WrappedViewerSettings = Form.create({ name: 'settings' })(ViewerSettings);
const mapStateToProps = (state) => ({
    tiers: state.viewer.tiers,
    selectedConection: state.viewer.selectedConection,
    settings: state.viewer.settings,
    el_settings: state.viewer.el_settings,
    globePoints: state.viewer.globePoints
});
const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        onResetSettings,
        onSaveSettings,
        changeLineSettings,
        changeSettings
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(WrappedViewerSettings);
