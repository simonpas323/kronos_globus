import React from 'react';
import { Slider, Switch, Form, Card, Input, Select, Button } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeSettings } from '../../../../redux/actions/viewer';

export class ConnectionSettings extends React.Component {

    onChange = (key, val, setting = 'connection') => {
        this.props.changeSettings({
            ...this.props.settings,
            [ setting ]: {
                ...this.props.settings[ setting ],
                [ key ]: val
            }
        });
    };

    render() {
        const { connection } = this.props.settings;
        const { opacity, visible, height, color, pointColor, pointOpacity, pointScale, pointSpeed } = connection;
        const { getFieldDecorator } = this.props.form;

        return (
            <div className={ 'row d-inline' }>

                <div className={ 'col-6 p-x' }>
                    <div className={ 'form-item' }>
                        <label>Line Height</label>
                        <Input
                            defaultValue={ height }
                            step={ 0.1 }
                            onChange={ (e) => this.onChange('height', e.target.value) }
                            type={ 'number' }
                            min={ 0 }
                        />
                    </div>
                    <div className={ 'form-item d-inline' }>
                        <label className={ ' col-5' }>Line Point Size</label>
                        <div className={ 'slider col-7' }>
                            <Slider
                                defaultValue={ pointScale }
                                onChange={ (val) => this.onChange('pointScale', val) }
                            />
                            <div className={ 'd-flex s-b text-info' }>
                                <span>0</span>
                                <span>100</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={ 'col-6 p-x' }>
                    <div className={ 'form-item' }>
                        <label>Line Point Color</label>
                        <Input
                            defaultValue={ pointColor }
                            onChange={ (e) => this.onChange('pointColor', e.target.value) }
                            type={ 'color' }
                        />
                    </div>
                    <div className={ 'form-item d-inline' }>
                        <label className={ ' col-5' }>Line Point Opacity</label>
                        <div className={ 'slider col-7' }>
                            <Slider
                                defaultValue={ pointOpacity }
                                onChange={ (val) => this.onChange('pointOpacity', val) }
                            />
                            <div className={ 'd-flex s-b text-info' }>
                                <span>0</span>
                                <span>100</span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        )
    }
}

const WrappedConnectionSettings = Form.create({ name: 'settings' })(ConnectionSettings);
const mapStateToProps = (state) => ({
    tiers: state.viewer.tiers,
    selectedConection: state.viewer.selectedConection,
    settings: state.viewer.settings,
    globePoints: state.viewer.globePoints
});
const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        changeSettings
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(WrappedConnectionSettings);
