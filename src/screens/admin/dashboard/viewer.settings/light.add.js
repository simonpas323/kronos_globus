import React from 'react';
import { Form,   Input, Select, Button, Slider } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeSettings, changeLineSettings } from '../../../../redux/actions/viewer';
import { Vector3 } from 'three-full';
import LightListSettings from './light.list';
import Card from './../../../../components/Card';
import   '../index.scss';

const Option = Select.Option;
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};

export class LightAddSettings extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.ligthsTypes[ 0 ].id,
            color: '#fefefe',
            intensity: 0.5,
            position: new Vector3(0, 100, 0),
        }
    }

    onChange = (key, value, position) => {
        if (position) {
            this.setState((s) => {
                return {
                    ...s,
                    position: {
                        ...s.position,
                        [ key ]: value
                    }
                }
            })
        } else {
            this.setState({
                [ key ]: value
            });
        }

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.changeSettings({
                    ...this.props.settings,
                    lights: [
                        ...this.props.settings.lights,
                        {
                            ...(this.props.ligthsTypes.filter((el) => el.id === this.state.id)[ 0 ]),
                            ...this.state,
                            uuid: Date.now()
                        }
                    ]
                });
            }
        });
    };

    render() {
        const { lights } = this.props.settings;
        const { getFieldDecorator } = this.props.form;
        const { color,intensity } = this.state;
        const { ligthsTypes } = this.props;

        return (

            <Card title="Lights" className={ 'col-12' }>
                <Form { ...formItemLayout }
                      onSubmit={ this.handleSubmit }
                      style={ { overflowY: 'auto', 'maxHeight': 'calc(100vh - 110px)' } }>

                    <div className={ 'row' }>
                        <div className={ 'col-6' }>
                            <h3 className={ 'light-title' }>Add Light</h3>
                            <div className={ 'form-item row  d-inline' }>
                                <label  >Select light type</label>
                                <Select
                                    defaultValue={ ligthsTypes[ 0 ].id }
                                    onChange={ (v) => this.onChange('id', v) }
                                >
                                    {
                                        ligthsTypes.map(light => <Option key={ light.id }
                                                                         value={ light.id }>{light.name}</Option>)
                                    }
                                </Select>
                            </div>
                            <div className={ 'row' }>
                                <div className={ 'col-6  px-1' }  >
                                    <div className={ 'form-item' }>
                                        <label>Color</label>
                                        <Input
                                            defaultValue={ color }
                                            onChange={ (e) => this.onChange('color', e.target.value) }
                                            type={ 'color' }
                                        />
                                    </div>
                                </div>
                                <div className={ 'col-6 px-2 ' } >
                                    <div className={ 'form-item' }>
                                        <label>Intensity</label>
                                        <Input
                                            step={ 0.1 }
                                            defaultValue={ intensity }
                                            onChange={ (e) => this.onChange('intensity', e.target.value) }
                                            type={ 'number' }
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className={ 'row' }>
                                <div className={ 'col-6   px-1' }  >
                                    <div className={ 'form-item' }>
                                        <label>Position x</label>
                                        <Input
                                            defaultValue={ this.state.position.x }
                                            onChange={ (e) => this.onChange('x', e.target.value, 'position') }
                                            type={ 'number' }
                                        />
                                    </div>
                                </div>
                                <div className={ 'col-6  px-2 ' }  >
                                    <div className={ 'form-item' }>
                                        <label>Position y</label>
                                        <Input
                                            step={ 0.1 }
                                            defaultValue={ this.state.position.y }
                                            onChange={ (e) => this.onChange('y', e.target.value, 'position') }
                                            type={ 'number' }
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className={ 'row' }>
                                <div className={ 'col-12 p-0  ' }  >
                                    <div className={ 'col-6   px-1' } style={ { padding:0, paddingRight:5 } }>
                                        <div className={ 'form-item' }>
                                            <label>Position z</label>
                                            <Input
                                            defaultValue={ this.state.position.z }
                                            onChange={ (e) => this.onChange('z', e.target.value, 'position') }
                                            type={ 'number' }
                                        />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={ 'row' }>
                                <div className={ 'col-6   px-1 ' } style={ {   paddingTop:15 } }>
                                    <Button type="primary" htmlType="submit">
                                        Add light
                                    </Button>
                                </div>
                            </div>

                        </div>
                        <div className={ 'col-6' }>
                            <h3 className={ 'light-title' }>Scene Lights</h3>
                            <LightListSettings/>
                        </div>
                    </div>

                </Form>
            </Card>

        )
    }
}

const WrappedViewerSettings = Form.create({ name: 'settings' })(LightAddSettings);
const mapStateToProps = (state) => ({
    ligthsTypes: state.viewer.ligthsTypes,
    settings: state.viewer.settings,
});
const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        changeLineSettings,
        changeSettings
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(WrappedViewerSettings);
