import React from 'react';
import { shallow } from 'enzyme';
import ViewerSettings from   './index.js';

describe('ViewerSettings', () => {
    it('ViewerSettings render', () => {

        const wrapper = shallow(<ViewerSettings />);
        expect(wrapper).toMatchSnapshot();
    });
});
