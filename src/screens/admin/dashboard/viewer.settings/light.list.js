import React from 'react';
import { Slider, Switch, Form, Card, Input, Select, Icon, Table } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeSettings } from '../../../../redux/actions/viewer';
import '../index.scss';

const { Column, ColumnGroup } = Table;

export class LightListSettings extends React.Component {

    updateLights = () => {
        this.props.changeSettings({
            ...this.props.settings,
            lights: [
                ...this.props.settings.lights,
            ]
        });
    }
    deleteLight = (light) => {
        this.props.changeSettings({
            ...this.props.settings,
            lights: this.props.settings.lights.filter((e) => e.uuid !== light.uuid)
        });
    };

    render() {
        const { lights } = this.props.settings;

        if (lights.length === 0) return null;
        return (
            <Table dataSource={ lights } key={ Math.random() } pagination={ false }>
                <Column
                    title="Name"
                    dataIndex="name"
                    key="name"
                />
                <Column
                    title="Color"
                    dataIndex="color"
                    key="color"
                    render={ (text, item) => (
                        <Input
                            defaultValue={ item.color }
                            onChange={ (e) => {
                                {
                                    item.color = e.target.value;
                                    this.updateLights();
                                }
                            } }
                            type={ 'color' }
                        />
                    ) }
                />
                <Column
                    title="Intensity"
                    dataIndex="intensity"
                    key="intensity"
                    render={ (text, item) => (
                        <Input
                            step={ 0.1 }
                            defaultValue={ item.intensity }
                            onChange={ (e) => {
                                {
                                    item.intensity = e.target.value;
                                    this.updateLights();
                                }
                            } }
                            type={ 'number' }
                        />
                    ) }
                />

                <ColumnGroup title="Position">
                    <Column
                        title="x"
                        dataIndex="x"
                        key="x"
                        render={ (tags, item) => (
                            item.id !== 1 ? (
                                <Input
                                    step={ 0.1 }
                                    defaultValue={ item.position.x }
                                    onChange={ (e) => {
                                        {
                                            item.position.x = e.target.value;
                                            this.updateLights();
                                        }
                                    } }
                                    type={ 'number' }
                                />
                            ) : null
                        ) }
                    />
                    <Column
                        title="y"
                        dataIndex="y"
                        key="y"
                        render={ (tags, item) => (
                            item.id !== 1 ? (
                                <Input
                                    step={ 0.1 }
                                    defaultValue={ item.position.y }
                                    onChange={ (e) => {
                                        {
                                            item.position.y = e.target.value;
                                            this.updateLights();
                                        }
                                    } }
                                    type={ 'number' }
                                />
                            ) : null
                        ) }
                    />
                    <Column
                        title="z"
                        dataIndex="z"
                        key="z"
                        render={ (tags, item) => (
                            item.id !== 1 ? (
                                <Input
                                    step={ 0.1 }
                                    defaultValue={ item.position.z }
                                    onChange={ (e) => {
                                        {
                                            item.position.z = e.target.value;
                                            this.updateLights();
                                        }
                                    } }
                                    type={ 'number' }
                                />
                            ) : null
                        ) }
                    />

                </ColumnGroup>
                <Column
                    title=""
                    key="action"
                    render={ (text, record) => (
                        <Icon type="close" onClick={ () => this.deleteLight(record) }/>
                    ) }
                />
            </Table>
        )
    }
}

const mapStateToProps = (state) => ({
    settings: state.viewer.settings,
});
const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        changeSettings
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(LightListSettings);
