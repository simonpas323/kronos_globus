import React from 'react';
import GlobeViewer from '../../../components/globe.viewer';
import ViewerSettings from './viewer.settings';
import Card from '../../../components/Card';
import   './index.scss';

export class Dashboard extends React.Component {

    render() {

        return (

            <div >
                <div className="gutter-row  col-4" >
                    <div className="globe-viewer"  >
                        <Card title={ 'Preview' } >
                            <GlobeViewer admin/>
                        </Card>

                    </div>
                </div>

                <div className="gutter-row col-8"   >

                    <ViewerSettings/>
                </div>

            </div>

        )
    }
}

export default Dashboard;
