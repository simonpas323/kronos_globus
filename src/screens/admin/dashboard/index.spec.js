import React from 'react';
import { shallow } from 'enzyme';
import Dashboard from   './index.js';

describe('Dashboard', () => {


    it('Dashboard render', () => {

        const wrapper = shallow(<Dashboard />);
        expect(wrapper).toMatchSnapshot();
    });
});
