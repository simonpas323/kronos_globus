import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Dashboard from './dashboard';
import { load } from '../../redux/actions/user';
import { resetSettings } from '../../redux/actions/viewer';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Login from './login';
import { Layout } from 'antd';
import './index.scss';
const { Content, Header, Footer } = Layout;

class Admin extends Component {

    constructor(p){
        super(p);
        this.state={
            reseted:false
        }
    }
    componentDidMount() {
        this.props.resetSettings();
        this.props.load();
        setTimeout(()=>{
            this.setState({
                reseted:true
            })
        },100)
    }

    render = () => {
        return (
            <Router>
                {
                    !this.props.auth ? (
                        <Login/>
                    ) : (
                        <Layout style={ { background: '#fff', padding: 0 } } >
                            <Header  >
                                <span>INTERFACE SETTINGS</span>
                                <span>Customize the interface for your convenience.</span>
                            </Header>
                            {
                                this.state.reseted && (
                                    <Content
                                        className={ 'main-admin-container' }
                                        style={ {
                                            overflow: 'initial',
                                        } }
                                    >
                                        <Switch>
                                            <Route exact path="" component={ Dashboard }/>
                                        </Switch>
                                    </Content>
                                )
                            }

                        </Layout>
                    )
                }

            </Router>
        );
    }
}

const mapStateToProps = (state) => ({
    auth: state.user.user.userName
});
const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        resetSettings,
        load
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(Admin);
