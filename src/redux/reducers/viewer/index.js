import actionTypes from '../../actions/actionTypes';

const VERSION = '_13';
const SETTINGS_KEY = `SETTINGS_KEY${ VERSION }`;
const SETTINGS_EL_KEY = `SETTINGS_EL_KEY${ VERSION }`;
let defaulSettings = localStorage.getItem(SETTINGS_KEY);
if (defaulSettings) {
    defaulSettings = JSON.parse(defaulSettings);
    defaulSettings.connection.pointColor = '#ff0000';
    defaulSettings.connection.pointOpacity = 70;
    defaulSettings.connection.pointSpeed = 100;
    defaulSettings.connection.pointScale = 5;
    defaulSettings.connection.height = 0.2;
}
let defaulElSettings = localStorage.getItem(SETTINGS_EL_KEY);
if (defaulElSettings) defaulElSettings = JSON.parse(defaulElSettings);

const DEF_EL_SETTINGS = {};
const DEF_TIER_SETTINGS = {
    color: '#ff0000',
    opacity: 1,
    speed: 1,
    scale: 0.03
};
const DEF_SETTINGS = {
    main: {
        pointRotateSpeed: 100,
        earthRotateSpeed: 0.1,
        meridianColor: '#1d2035',
        meridianVisible: true,
        meridianOpacity: 20,
        countryBackColor: '#ffffff',
        countryColor: '#ffffff',
        countryOpacity: 100,
        tierFilter: 'ALL',
        countryFilter: 'US',
        backgroundColor: '#ffffff',
        countOfActive: 2,
        oceanColor: '#548ba4',
        oceanOpacity: 10
    },
    lights: [
        {
            id: 1,
            uuid: 1,
            color: '#e1e1e1',
            name: 'AreaLight',
            intensity: 0.7,
        },
        {
            id: 2,
            uuid: 2,
            color: '#e1e1e1',
            name: 'DirectionLight',
            intensity: 0.4,
            position: {
                x: 0,
                z: 0,
                y: 1
            },
        }
    ],
    connection: {
        color: '#ff0000',
        height: 0.2,
        visible: true,
        opacity: 100,
        pointColor: '#ff0000',
        pointOpacity: 70,
        pointSpeed: 100,
        pointScale: 5
    },
    controls: {
        rotateSpeed: 0.1
    }
};

function _initialState() {
    return {
        loaded: false,
        selectedConection: null,
        ligthsTypes: [
            {
                id: 1,
                name: 'AreaLight'
            },
            {
                id: 2,
                name: 'DirectionLight'
            },
            {
                id: 3,
                name: 'SpotLight'
            }
        ],
        tiers: [],
        countries: [],
        globePoints: [],
        dataPoints: {
            nodes: [],
            links: []
        },

        el_settings: defaulElSettings || JSON.parse(JSON.stringify(DEF_EL_SETTINGS)),
        tier_settings: JSON.parse(JSON.stringify(DEF_TIER_SETTINGS)),
        settings: JSON.parse(JSON.stringify(defaulSettings || DEF_SETTINGS))
    }
}

const initialState = _initialState();
const viewer = (state = { ...initialState }, action) => {
    switch (action.type) {
        case actionTypes.VIEWER_LOAD_SETTINGS: {
            defaulSettings = action.payload.settings;
            defaulElSettings = action.payload.el_setings;
            defaulSettings.connection.pointColor = '#ff0000';
            defaulSettings.connection.pointOpacity = 70;
            defaulSettings.connection.pointSpeed = 100;
            defaulSettings.connection.pointScale = 5;
            defaulSettings.connection.height = 0.2;
            return{
                ...state,
                settings: action.payload.settings,
                el_settings: action.payload.el_setings
            }
        }
        case actionTypes.VIEWER_CHANGE_ELEMENT_SETTINGS: {

            const settings = state.el_settings[ action.payload ] || state.settings.connection;
            return {
                ...state,
                selectedConection: action.payload,
                el_settings: {
                    ...state.el_settings,
                    [ action.payload ]: settings
                },
                settings: {
                    ...state.settings,
                    connection: {
                        ...settings
                    }
                }
            }
        }
        case actionTypes.VIEWER_SAVE_SETTINGS: {
            // localStorage.setItem(SETTINGS_KEY, JSON.stringify(state.settings));
            // localStorage.setItem(SETTINGS_EL_KEY, JSON.stringify(state.el_settings));
            return state;
        }
        case actionTypes.VIEWER_RESET_SETTINGS: {
            // localStorage.removeItem(SETTINGS_KEY);
            // localStorage.removeItem(SETTINGS_EL_KEY);
            return {
                ...state,
                el_settings: JSON.parse(JSON.stringify(DEF_TIER_SETTINGS)),
                settings: JSON.parse(JSON.stringify(DEF_SETTINGS)),
            };
        }
        case actionTypes.VIEWER_CHANGE_SETTINGS: {

            if (state.selectedConection !== 'NONE' && state.el_settings[ state.selectedConection ]) {
                state.el_settings[ state.selectedConection ] = {
                    ...action.payload.connection
                }
            }

            return {
                ...state,
                settings: {
                    ...state.settings,
                    ...action.payload
                }
            }
        }
        case actionTypes.VIEWER_UPDATE: {

            return {
                ...state,
                settings: {
                    ...state.settings,
                    ...action.payload
                }
            }
        }
        case actionTypes.VIEWER_RESET: {

            return {
                ...(_initialState()),
                dataPoints:state.dataPoints,
                globePoints:state.globePoints,
                tiers:state.tiers,
                loaded:state.loaded,
            }
        }
        case actionTypes.VIEWER_CONNECTION_METADATA: {
            const { dataPoints } = action.payload;
            if (dataPoints) {
                const tiers = [];
                action.payload.tiers = [ {
                    id: 'ALL'
                } ];
                action.payload.loaded = true;
                action.payload.globePoints = dataPoints.links.map((link, index) => {
                    let buyer, seller;
                    for (let i = 0; i < dataPoints.nodes.length; i++) {
                        if (dataPoints.nodes[ i ].seller_id === link.seller_id) {
                            seller = dataPoints.nodes[ i ];
                            if (buyer) break;
                        } else if (dataPoints.nodes[ i ].seller_id === link.buyer_id) {
                            buyer = dataPoints.nodes[ i ];
                            if (seller) break;
                        }
                    }

                    if (
                        !seller ||
                        !buyer ||
                        !seller.address ||
                        !seller.address.coords ||
                        !buyer.address ||
                        !buyer.address.coords
                    // (seller.address.coords.lat - buyer.address.coords.lat < DELTA && seller.address.coords.lon - buyer.address.coords.lon < DELTA)
                    ) return false;
                    if (typeof link.tier !== 'undefined' && tiers.indexOf(link.tier) < 0) {
                        tiers.push(link.tier);
                        action.payload.tiers.push({
                            id: link.tier,
                            settings: DEF_TIER_SETTINGS
                        });
                    }
                    return {
                        id: `el-${ index }`,
                        ...link,
                        connectName: `${ seller.seller_name }-${ buyer.seller_name }`,
                        start: {
                            ...seller.address.coords
                        },
                        end: {
                            ...buyer.address.coords
                        }

                    }
                }).filter((el) => el) ;//.splice(0,10);
            }

            return {
                ...(_initialState()),
                ...action.payload
            }
        }
        default:
            return state;
    }
};

export default viewer;
