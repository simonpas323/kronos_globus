import actionTypes from '../../actions/actionTypes';

const initialState = {
    user: {},
};
const user = (state = { ...initialState }, action ) => {
    switch (action.type) {
        case actionTypes.USER_FETCH: {
            return {
                ...state,
                ...action.payload,
            };
        }

        default:
            return state;
    }
};

export default user;
