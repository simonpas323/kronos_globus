import { combineReducers } from 'redux';

import user from './reducers/user';
import viewer from './reducers/viewer';

const rootReducer = combineReducers({
    viewer,
    user
});

export default rootReducer;
