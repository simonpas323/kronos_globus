const actionTypes = {
    USER_FETCH:'USER_FETCH',
    VIEWER_SAVE_SETTINGS:'VIEWER_SAVE_SETTINGS',
    VIEWER_RESET_SETTINGS:'VIEWER_RESET_SETTINGS',
    VIEWER_LOAD_SETTINGS:'VIEWER_LOAD_SETTINGS',
    VIEWER_CHANGE_SETTINGS:'VIEWER_CHANGE_SETTINGS',
    VIEWER_UPDATE:'VIEWER_UPDATE',
    VIEWER_RESET:'VIEWER_RESET',
    VIEWER_CHANGE_ELEMENT_SETTINGS:'VIEWER_CHANGE_ELEMENT_SETTINGS',
    VIEWER_CONNECTION_METADATA:'VIEWER_CONNECTION_METADATA'
}

export default actionTypes
