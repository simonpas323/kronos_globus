import actionTypes from '../actionTypes';
import axios from 'axios';
import { FBXLoader } from 'three-full';

// const URL = 'http://localhost:3366/settings';
const URL = 'https://doormap.app/test/';

export const changeSettings = (settings) => (dispatch) => {
    dispatch({
        type: actionTypes.VIEWER_CHANGE_SETTINGS,
        payload: settings
    });

};
export const loadSettings = () => (dispatch) => {
    return new Promise((resolve) => {
        axios.get(`${ URL }`).then((res) => {
            if (res.data.data) {
                dispatch({
                    type: actionTypes.VIEWER_LOAD_SETTINGS,
                    payload: res.data.data.settings
                });
            }
            resolve();
        });
    })

}

export const updateView = (settings) => (dispatch) => {
    dispatch({
        type: actionTypes.VIEWER_UPDATE,
        payload: settings
    });

};
export const changeLineSettings = (settings) => (dispatch) => {
    dispatch({
        type: actionTypes.VIEWER_CHANGE_ELEMENT_SETTINGS,
        payload: settings
    });

};
export const resetSettings = () => (dispatch) => {
    dispatch({
        type: actionTypes.VIEWER_RESET
    });

};
export const onSaveSettings = (settings) => (dispatch) => {
    axios.post(`${ URL }`, settings);
};
export const onResetSettings = (settings) => (dispatch) => {
    axios.delete(`${ URL }`).then((res) => {
        dispatch({
            type: actionTypes.VIEWER_RESET_SETTINGS,
            payload: res.data
        });
    });
};

export const loadMetaData = () => (dispatch) => {

    const urls = [
        'assets/capital_one_supply_chain.json',
        // 'assets/capital_one_messy_supply_chain.json',
    ];
    return new Promise((resolve) => {
        Promise.all(urls.map(url =>
            fetch(url).then(resp => resp.json())
        )).then(dataPoints => {
            const nodes = [], links = [];
            dataPoints.forEach((el) => {
                nodes.push(...el.nodes);
                links.push(...el.links);
            });
            dispatch({
                type: actionTypes.VIEWER_CONNECTION_METADATA,
                payload: {
                    dataPoints: {
                        nodes,
                        links
                    }
                }
            });
        }).catch((e) => {
            console.log(e);
        })
            .finally(resolve);
    })

};
