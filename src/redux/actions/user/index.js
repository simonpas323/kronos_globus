import actionTypes from '../actionTypes';

const USEE_KEY = 'USER_KEY_1';

export const load = () => async (dispatch) => {
    let user = localStorage.getItem(USEE_KEY);
    if (user) {
        user = JSON.parse(user);
    } else {
        user = {};
    }
    dispatch({
        type: actionTypes.USER_FETCH,
        payload: { user }
    })

};
export const logIn = (user) => async (dispatch) => {
    localStorage.setItem(USEE_KEY, JSON.stringify(user));

    dispatch({
        type: actionTypes.USER_FETCH,
        payload: { user }
    })

};
