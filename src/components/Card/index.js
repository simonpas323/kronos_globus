import React from 'react';
import './index.scss';

export default function Card(props) {
    return (
        <div className={ `${ props.className } `  } style={ props.style }>
            <div className={ 'my-card ' }>
                <div className={ 'title' }>{props.title}</div>
                <div className={ 'content' }>{props.children}</div>
            </div>
        </div>
    );

}
