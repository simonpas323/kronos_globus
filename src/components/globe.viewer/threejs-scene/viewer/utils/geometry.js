import {
    BufferGeometry,
    BufferAttribute
} from 'three-full';

export default class LineBufferGeometry extends BufferGeometry {
    constructor(path, segments) {
        super();

        this.path = path;
        this.segments = segments;

        this.addAttribute('position', new BufferAttribute(new Float32Array(this.segments * 3), 3));

        let index = 0;
        const points = this.path.getPoints(this.segments - 1);
        const positions = this.attributes.position.array;

        let distance = 0;

        let previous = new Vector3();

        for (let i = 0, l = this.segments; i < l; i++) {
            positions[ index++ ] = points[ i ].x;
            positions[ index++ ] = points[ i ].y;
            positions[ index++ ] = points[ i ].z;

            distance += previous.distanceTo(points[ i ]);

            previous = points[ i ];
        }

        this.attributes.position.needsUpdate = true;

        this.count = this.attributes.position.count;
        this.distance = distance;
    }
}
