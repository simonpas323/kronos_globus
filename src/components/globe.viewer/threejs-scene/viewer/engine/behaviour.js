import async from 'async';
import {
    Group,
} from 'three-full';

import Events from './events';
import Engine from './index';

let broadcast = null;
let engine = null;
let scene = null;
const gui = null;

const behaviours = {};

export default class Behaviour extends Events {
    constructor(...args) {
        super();

        this.object = args[ 1 ];
        if (args[ 0 ] && engine === null) {
            engine = this.engine = new Engine(args[ 0 ]);

            scene = new Group();
            scene.name = 'Hierarchy';

            engine.scene.add(scene);

            broadcast = new Events();
        }

        behaviours[ this.constructor.name ] = behaviours[ this.constructor.name ] || 0;
        behaviours[ this.constructor.name ]++;

        const count = behaviours[ this.constructor.name ];

        ////

        this.broadcast = broadcast;
        this.engine = engine;
        this.scene = scene;
        this.gui = gui;

        this.camera = engine.camera;
        this.controls = engine.controls;
        this.renderer = engine.renderer;
        this.raycaster = engine.raycaster;

        ////

        this.props = {};

        // this.folder = this.gui.addFolder(this.constructor.name + (count > 1 ? count : ''));
        // this.folder.open();

        ////

        this.init(...args);

        const funcs = this.before();
        const done = funcs.pop();

        async.eachSeries(funcs, (func, next) => {
            func.call(this, next);
        }, (err) => {
            if (done) {
                done.call(this, err);
            }

            this.start();
            this.ui();
            this.listen();
        });
    }

    destroy() {
        engine = null;
    }

    init() {

    }

    before() {
        return [];
    }

    start() {

    }

    ui() {

    }

    listen() {

    }
}
