import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Viewer from './viewer';
import { connect } from 'react-redux';

import { changeSettings } from '../../../redux/actions/viewer';
import './index.scss';
import { bindActionCreators } from 'redux';

class ThreeJSScene extends Component {
    static propTypes = {
        viewer: PropTypes.shape({
            globePoints: PropTypes.any.isRequired,
        })
    };

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.viewer.settings) {
            if(this.viewer)this.viewer.applySettings(nextProps.viewer);
        }
    }

    componentDidMount() {
        this.viewer = new Viewer(this.container, this.props.object);
        this.viewer.applySettings(this.props.viewer);
        this.viewer.on('changeLight', (light) => {

            this.props.changeSettings({
                ...this.props.viewer.settings,
                lights: this.props.viewer.settings.lights.map((el) => {
                    if (el.uuid === light._settings.uuid) {
                        el.position = light._globalPosition;
                    }
                    return el
                })
            });
        })

    }

    componentWillUnmount() {
        this.viewer.onDestroy();
    }

    render() {
        return (
            <div
                className="threejs-scene"
                ref={ (node) => {
                    this.container = node
                } }
            />
        );
    }
}

const mapStateToProps = (state) => ({
    viewer: state.viewer
});
const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        changeSettings
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(ThreeJSScene)
