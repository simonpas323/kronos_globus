import React, { Component } from 'react';
import ThreeJSScene from './threejs-scene';
import './index.scss';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadMetaData, resetSettings, loadSettings } from '../../redux/actions/viewer';
import Loader from '../../components/loader';
import { FBXLoader, Object3D } from 'three-full';
import GlobeDefaultControls from './globe.default-controls';

let globe = null;

export class GlobeViewer extends Component {
    componentDidMount() {
        this.props.resetSettings();
        this.props.loadSettings().then(() => {
            if (globe) {
                this.object = new Object3D();
                globe.forEach((el) => {
                    this.object.add(el);
                });

                this.setState({
                    modelLoaded: true
                });
            } else {
                (new FBXLoader()).load('assets/glob1.fbx', (object) => {
                    this.object = object;
                    globe = [ ...object.children ];
                    this.props.loadMetaData();
                });
            }
        });
    }

    render = () => {
        const { admin, mapsLoaded } = this.props;
        const view = mapsLoaded  ? (
            <div className="viewer">
                {!admin && (<GlobeDefaultControls/>)}
                {
                    this.object && <ThreeJSScene object={ this.object }/>
                }
            </div>
        ) : (
            <Loader/>
        );

        return view;
    }
}

const mapStateToProps = (state) => ({
    mapsLoaded: state.viewer.loaded,
});
const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        loadSettings,
        resetSettings,
        loadMetaData
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(GlobeViewer)
