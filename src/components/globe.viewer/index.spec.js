import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store'
import { Provider } from 'react-redux';
import GlobeViewerConnected, { GlobeViewer } from './index.js';
import {   Object3D } from 'three-full';

const initialState = { output:100 };
const mockStore = configureStore();
let store,container;
const props = {
    loadMetaData: () => new Promise(() => false),
    resetSettings: () => new Promise(() => false),
    loadSettings: () => new Promise(() => false)
};
let wrapper,wrapperConnected;
describe('GlobeViewer', () => {
    beforeEach(() => {
        store = mockStore(initialState);
        wrapper = shallow(<GlobeViewer { ...props } />);
        // wrapperConnected = shallow(<Provider ><GlobeViewerConnected { ...props } /></Provider>);
    });

    it('GlobeViewer show have Loader', () => {
        expect(wrapper.find('Loader').length).toBe(1);
    });

    it('GlobeViewer should have ThreeJSScene', () => {
        wrapper.setProps({
            mapsLoaded:true
        });
        wrapper.setState({
            modelLoaded:true
        });
        wrapper.instance().object = new Object3D();
        expect(wrapper.find('ThreeJSScene').length).toBe(1);
    });

    it('GlobeViewer should have Viewer controls', () => {
        wrapperConnected.setProps({
            admin:false,
            mapsLoaded:true
        });
        wrapperConnected.setState({
            modelLoaded:true
        });
        // expect(wrapper).toMatchSnapshot();
        expect(wrapperConnected.find('GlobeDefaultControls').length).toBe(1);
    });
});
