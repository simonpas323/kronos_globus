import React from 'react';
import './index.scss';
import logo from '../../../assets/img/Logo.png';

export default function Logo() {

    return (
        <div className={ 'viewer-logo' }>
            <img src={ logo }/>
        </div>
    );
}
