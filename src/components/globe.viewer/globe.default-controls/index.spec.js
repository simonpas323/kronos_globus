import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store'
import GlobeDefaultControls,{GlobeDefaultControlsComponent}  from './index.js';

import Logo from '../Logo';
const initialState = { output:100 };
const mockStore = configureStore();
let store,container;
const props = {
    settings:{
        main:{
            tierFilter:''
        }
    },
    tiers: [],
    countries: [],
    loaded:true,
    loadSettings: () => new Promise(() => false)
};
let wrapper;

describe('GlobeDefaultControls', () => {
    beforeEach(() => {
        store = mockStore(initialState);
        wrapper = shallow(<GlobeDefaultControlsComponent { ...props } />);
    });

    it('GlobeDefaultControls should be focused on filter by count', () => {
        expect(wrapper.find('.centered.count-filter').length).toBe(1);
        expect(wrapper.containsMatchingElement(<Logo></Logo>)).toBeTruthy();
    });

    it('GlobeDefaultControls should be focused on filter by tiers', () => {
        wrapper.setState({
            activeEl:0
        });
        expect(wrapper.find('.centered.tier-filter').length).toBe(1);
    });

});
