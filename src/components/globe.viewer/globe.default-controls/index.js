import React, { Component } from 'react';
import './index.scss';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateView } from '../../../redux/actions/viewer';
import { Slider, Icon, Select } from 'antd';
import Logo from '../Logo';
import Iconarm from '../../../assets/img/Iconarm.png';

export class GlobeDefaultControlsComponent extends Component {
    state = {
        loaded: false,
        activeEl: 1
    };

    onChange = (key, val, setting = 'connection') => {
        this.props.updateView({
            ...this.props.settings,
            [ setting ]: {
                ...this.props.settings[ setting ],
                [ key ]: val
            }
        });
    };
    slideTo = (dir) => {
        this.setState({
            activeEl: !this.state.activeEl
        })
    };
    render = () => {
        return (
            <div  >
                <Logo/>
                <div className={ 'middle-settings' }>
                    <div
                        className={ ` filter-tier filter-container tier-filter ${ this.state.activeEl == 0 ? 'centered' : '' }` }>
                        <Select
                            value={ this.props.settings.main.tierFilter }
                            onChange={ (val) => this.onChange('tierFilter', val, 'main') }
                        >
                            {
                                this.props.tiers.map((tier) => {
                                    return (
                                        <Select.Option key={ tier.id } value={ tier.id }>{tier.id}</Select.Option>
                                    )
                                })
                            }
                        </Select>
                        <div className={ 'filter-text' }>Filter by Tiers</div>
                    </div>

                    <div
                        className={ `filter-count filter-container CONTAINER count-filter ${ this.state.activeEl == 1 ? 'centered' : '' }` }>
                        <Slider
                            defaultValue={ this.props.settings.main.countOfActive }
                            onChange={ (val) => this.onChange('countOfActive', val, 'main') }
                        />
                        <div className={ 'filter-text' }>Count of connections</div>
                    </div>

                    <div className={ 'slider-controls' }>
                        <Icon type="left" onClick={ () => this.slideTo(-1) }/>
                        <Icon type="right" onClick={ () => this.slideTo(1) }/>
                    </div>
                </div>
                <div className={ 'drag-helper' }>
                    <img src={ Iconarm }/>
                    <p>
                        Grab and spin the globe to interact,<br/>
                        scroll to increase or reduce.
                    </p>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    tiers: state.viewer.tiers,
    countries: state.viewer.countries,
    loaded: state.viewer.loaded,
    settings: state.viewer.settings
});
const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        updateView
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(GlobeDefaultControlsComponent)
