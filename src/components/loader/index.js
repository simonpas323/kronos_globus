import React from 'react';
import './index.scss';
// import LOGO from '../../assets/img/Logo.png';
// import LOGOEarth from '../../assets/img/Iconeworldwide.png';

export default function Loader() {
    return (
        <div className="lds-ripple">
            <div></div>
            <div></div>
        </div>
    )
}
