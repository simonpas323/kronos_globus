/// <reference types="Cypress" />

context('Home default page', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/#/admin');
        cy.get('input[type="text"]').type('swell');
        cy.get('input[type="password"]').type('swell');
        cy.get('button').click();
        cy.wait(1000);//wait until connections in viewer appier;
    });

    it('should apply settings for input on change tier', async () => {
        const selectionSelector = '.connections-card .ant-select-selection';
        const color = '#ff0000';
        const connectionColorPicker = '.connections-card input[type="color"]';

        cy.get(connectionColorPicker).then((colorElement) => {

            cy.get(selectionSelector)
                .invoke('attr', 'aria-controls')
                .then(async (ariaControls) => {
                    cy.get(selectionSelector).trigger('click');
                    cy.wait(1000);
                    const liItems = cy.get(`#${ ariaControls } li`);
                    liItems.eq(2).then((el) => {

                        el.trigger('click');
                        cy.wait(2000);//wait until connections in viewer appier;
                        cy.get(connectionColorPicker).then((colorElement) => {
                            Cypress.$(colorElement).val(color);//switch to second tier and set up new color

                            cy.wait(1000);//wait until connections in viewer appier;
                            cy.get(selectionSelector).trigger('click');

                            cy.get(selectionSelector)
                                .invoke('attr', 'aria-controls')
                                .then(async (ariaControls) => {
                                    const liItems = cy.get(`#${ ariaControls } li`);
                                    liItems.eq(1).then((el) => {
                                        el.trigger('click');

                                        cy.wait(3000);
                                        cy.get(connectionColorPicker).then((colorElement) => {
                                            Cypress.$(colorElement).val('#fefefe');//switch to third tier and set up the color

                                            cy.get(selectionSelector).trigger('click');
                                            cy.get(selectionSelector)
                                                .invoke('attr', 'aria-controls')
                                                .then(async (ariaControls) => {
                                                    cy.wait(3000);
                                                    const liItems = cy.get(`#${ ariaControls } li`);
                                                    liItems.eq(2).then((el) => {
                                                        el.trigger('click');
                                                        // console.log(cy.get(connectionColorPicker).val());
                                                        cy.wait(500);
                                                        cy.get(connectionColorPicker).should('have.value', color);
                                                    });
                                                });
                                        });
                                    });
                                });
                        })
                    });
                })
        })

    });
    // it('should render the viewer', () => {
    //
    // });
    // it('should add Spotlight light', () => {
    // });
    //
    // it('should edit Spotlight light', () => {
    // });

});
