/// <reference types="Cypress" />

context('Home default page', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000');
        cy.wait(5000);//wait until connections in viewer appier;
    });

    /*it('filter by tiering', () => {
        const selection = '.tier-filter .ant-select-selection';

        cy.get(selection).trigger('click');
        cy.wait(1000);
        cy.get(selection)
            .invoke('attr', 'aria-controls')
            .then((ariaControls) => {
                const liItems = cy.get(`#${ ariaControls } li`);

                liItems.eq(2).then((el) => el.trigger('click'));
            })
    });

    it('sort by count', () => {
        const selection = '.count-filter .ant-slider .ant-slider-handle';

        cy.get(selection)
            .trigger('mousedown', { position:'topLeft' })
            .trigger('mousemove', { position:'topLeft',duration: 2000 })
            .trigger('mouseup', { force: true });

    })*/
    it('sort by count', () => {
        cy.wait(5000);
        cy.window()
            .trigger('mousedown', { position:'bottomLeft',duration: 2000 })
            .trigger('mousemove', { position:'topRight',duration: 2000 })
            .trigger('mouseup', { force: true })
            .trigger('wheel', { force: true })

    })

})
